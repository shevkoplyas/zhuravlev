package ca.dimon.zhuravlev;

import java.util.Arrays;
import com.vcollaborate.bitwise.*; // "*" since we use both bitwise classes
import java.util.BitSet;

import java.io.File;
import java.io.FileNotFoundException;
import static java.lang.System.in;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Zhuravlev {

    // Our input CSV files
    private String yes_gold_csv_file = "/home/dmitry/Downloads/yes_gold.10k_records.csv";
    private String no_gold_csv_file = "/home/dmitry/Downloads/no_gold.10k_records.csv";
    private int initial_test_size = 1;
    private int max_allowed_test_size = 15;

// demo 01:
//    int yes_gold_start_row = 1;
//    int yes_gold_end_row = 12;
//    int no_gold_start_row = 1;
//    int no_gold_end_row = 16;

    // demo 02:
    int yes_gold_start_row = 100;
    int yes_gold_end_row = 200;
    int no_gold_start_row = 100;
    int no_gold_end_row = 200;

//    private int verbosity = 0; // 0 = most silent, 1 = some debugging stats, 2 = more stats
    private int number_of_bits = 0; // value will be determened automagically by number of fields in the CSV file (CSV must have headers line)
    private String one_as_string = "1";

    // Create a BitSet by given String. Example: "1001" -> {0, 3}
    // https://stackoverflow.com/questions/33383294/large-string-of-1-and-0-to-bitset
    // ->
    // https://stackoverflow.com/a/18925602/7022062
    private static BitSet createFromString(String s) {
        BitSet t = new BitSet(s.length());
        int lastBitIndex = s.length() - 1;

        for (int i = lastBitIndex; i >= 0; i--) {
            if (s.charAt(i) == '1') {
                t.set(lastBitIndex - i);
            }
        }

        return t;
    }

    // Read given file_name 
    // startign from line having index start_row
    // ending with line having index end_row
    // into the List of Long values "values_list".
    // Note: each row of CSV file with N comma-separated bits is represented by 1 Long value in the values_list.
    //
    // Negagive start value means start from beginning.
    // Negagive end_row value means read till the end.
    //
    // https://stackoverflow.com/questions/40074840/reading-a-csv-file-into-a-array
    // ->
    // https://stackoverflow.com/a/40075446/7022062
    public void read_csv_file_into_long_array(String file_name, int start_row, int end_row, List<Long> values_list, List<String> field_name_array) {

        File file = new File(file_name);

        // Negagive start value means start from beginning
        if (start_row < 0) {
            start_row = 0;
        }

        // Negagive end_row value means read till the end
        if (end_row < 0) {
            end_row = Integer.MAX_VALUE; // by far big enough (will never 2 giga-rows table:)
        }

        Scanner inputStream;

        try {
            inputStream = new Scanner(file);

            // Initiate line index with -1, so on 1st iteration it will have proper value 0 and we can simply keep increasing it.
            int line_index = -1;

            while (inputStream.hasNext()) {
                // Calculate line index (1st line has line_index value == 0)
                line_index++;

                // Read the line from the file
                String line = inputStream.next();
                System.out.print("csv line: " + line);

                // Consume headers
                if (line_index == 0 && field_name_array != null) {
                    field_name_array.addAll(Arrays.asList(line.split(",")));
                    continue;
                }

                // Check if line_index is in the requested start/stop range user asked for.
                // Note: we ALWAYS skipping 1st row with line_index == 0 to skip headers
                if (line_index < start_row) {
                    // Skip line with index less than start_row
                    System.out.println(" ...skipping line...");
                    if (number_of_bits == 0) {
                        number_of_bits = line.split(",").length;
                    }
                    continue;
                }

                if (line_index > end_row) {
                    // Skip the rest of the lines with index above end_row
                    System.out.println(" ...skipping the rest of the lines...");
                    break;
                }

                // The line_index is inside the range of our interest, consume the line
                line = line.replace(",", "");      // remove commas from the line to turn it into binary
                long long_value = BinaryUtils.fromBitSet(createFromString(line));
                System.out.println(" -> " + long_value);
                values_list.add(long_value); // convert binary string to BitSet, then convert it into Long and add into the result list
            }
            System.out.println();

            inputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    // Just a kick-starter (Java require "main" function to be declared as "static")
    public static void main(String[] args) {
        Zhuravlev zhuravlev = new Zhuravlev();
        zhuravlev.main2();
    }

    // Actual main function (non-static member of the class having access to all the class instance members).
    public void main2() {
        // Let's read yes_gold & no_gold CSV files into arrays (we'll use List) of Long values (1 row = 1 Long value)
        List<Long> yes_gold_rows = new ArrayList<>();
        List<Long> no_gold_rows = new ArrayList<>();
        List<String> headers = new ArrayList<>();
        read_csv_file_into_long_array(yes_gold_csv_file, yes_gold_start_row, yes_gold_end_row, yes_gold_rows, headers);
        read_csv_file_into_long_array(no_gold_csv_file, no_gold_start_row, no_gold_end_row, no_gold_rows, null);

        // We'll store all the found tests here as a Long number represending found bitmask
        List<Long> found_tests = new ArrayList<>();

        System.out.println("yes rows count: " + yes_gold_rows.size());
        System.out.println("no rows count: " + no_gold_rows.size());
        System.out.println("number_of_bits detected: " + number_of_bits);

        // The "nextPermutation()" can generate up to 64 bits long permutations, but ofthen times our "number_of_bits" value
        // is much less than max 64 possible fields/properties. To stop iterating permutations we'll need to get a max possible
        // value for the "bit mask" of our permutations and as soon as "nextPermutation()" gives us value bigger than that max
        // we know we've exhausted all possible permutations inside "number_of_bits" subset/subrange.
        long max_permutation_size = (long) Math.pow(2, number_of_bits) - 1L;

        // For each test size starting with initial_test_size up to max_allowed_test_size
        for (int test_size = initial_test_size; test_size <= max_allowed_test_size; test_size++) {

            // Build initial_bit_combination_str string as repetition of "1" test_size times
            String initial_bit_combination_str = IntStream.range(0, test_size).mapToObj(i -> one_as_string).collect(Collectors.joining(""));

            // Initialize test_bitmask with 0 value, so we'll initialize it by initial_bit_combination_str on 1st all_permutations_loop iteration.
            long test_bitmask = 0;

            // Iterate all possible permutations of test_bitmask
            all_permutations_loop:
            for (long permutation_count = 0; permutation_count < Long.MAX_VALUE; permutation_count++) {

                // For each new permutation: unitialize blank hashmaps:
                //    - hashmap key is i-th row "test candidate" value
                //    - hashmap value is a count of rows with that (same) value
                //
                // Remember: a true "test" is a minimal bits combinations when none of corresponding values from yes-rows intersects with values from no-rows
                ConcurrentHashMap<Long, Integer> yes_test_values_hashmap = new ConcurrentHashMap<>();
                ConcurrentHashMap<Long, Integer> no_test_values_hashmap = new ConcurrentHashMap<>();

                // Generate next bitmask permutation
                test_bitmask = test_bitmask == 0
                        ? BinaryUtils.fromBitSet(createFromString(initial_bit_combination_str)) // Convert initial_bit_combination_str string representing a bit sequence into long
                        : BinaryUtils.nextPermutation(test_bitmask);  // generate next test_bitmask permutation

                // Check if it is not last permutation (-1) 
                // or if we have exceeded our max permutations (limited by size of number_of_bits)
                if (test_bitmask == -1 || test_bitmask > max_permutation_size) {
                    break all_permutations_loop;
                }

                // Print "new test_size" during 1st loop
                if (permutation_count == 0) {
                    System.out.println("\n----------- new test_size: " + test_size + " ----------------------------");
                    System.out.print(" test_bitmask: " + BinaryStringUtils.zeroPadString(Long.toBinaryString(test_bitmask), number_of_bits));
                    System.out.println(" (" + test_bitmask + ")");
                    System.out.println(" max_permutation_size: " + max_permutation_size);
                }

                // Print some stats from time to time as we go
                if (false) {
                    if (permutation_count % 100000000 == 0 || max_permutation_size < 100000000) {
                        System.out.print(BinaryStringUtils.zeroPadString(Long.toBinaryString(test_bitmask), number_of_bits));
                        System.out.print(" count: ");
                        System.out.print(permutation_count + 1); // +1 for humans show 1st line count as "1" (not as "0")
                        System.out.print(" test found: " + found_tests.size());
                        System.out.println();
                    }
                }

                // We got out next "test" candidate bitmask. Let's check if it is a test or not by
                // running in parralel both "yes" and "no" rows instead of reading all "yes" rows and then checking if it intersects with
                // andy of "no" rows (or vice versa).
                // Check if this "test candidate" overlaps with any of previously found shorter tests
                for (Long ith_found_test : found_tests) {
                    if ((ith_found_test & test_bitmask) == ith_found_test) {
                        // Smaller subset of current test candidate was already found as a true test earlier.
                        // Remember: we can't arbitrary increase the existing tests length. (test is a *minimal* set separating "yes" from "no" rows)
                        // Not a test!
                        continue all_permutations_loop;
                    }
                }

                // For each permutation of a test_bitmask read all yes and no rows (their count might differ!)
                // and check if any of values of "test to be" bitmask intersects between yes and no rows
                // (true "test" has no intersections and 100% separates "yes" rows from "no" rows.
                for (int row_index = 0; row_index < Math.max(yes_gold_rows.size(), no_gold_rows.size()); row_index++) {

                    // Check next "yes" row if index in range
                    if (row_index < yes_gold_rows.size()) {
                        // Get i-th "yes" row test candidate value
                        long yes_test_value = test_bitmask & yes_gold_rows.get(row_index);

                        // Check if any of "no" rows values have same value? (Then our "test" candidate failed - not a "test"!)
                        if (no_test_values_hashmap.containsKey(yes_test_value)) {
                            // Yes-value intersects with no-values, current test_bitmask isn't a test
                            // Not a test!
                            continue all_permutations_loop;
                        }

                        // Store value by incrementing count within corresponding HashMap (or initialize with count value 1
                        // if this value seen 1st time)   https://stackoverflow.com/a/31361400/7022062  
                        yes_test_values_hashmap.merge(yes_test_value, 1, (a, b) -> a + b);
                    }

                    // Check next "no" row if index in range
                    if (row_index < no_gold_rows.size()) {
                        // Get i-th "no" row test candidate value
                        long no_test_value = test_bitmask & no_gold_rows.get(row_index);

                        // Check if any of "yes" rows values have same value? (Then our "test" candidate failed - not a "test"!)
                        if (yes_test_values_hashmap.containsKey(no_test_value)) {
                            // No-value intersects with yes-values, current test_bitmask isn't a test
                            // Not a test!
                            continue all_permutations_loop;
                        }

                        // Store value by incrementing count within corresponding HashMap (or initialize with count value 1
                        // if this value seen 1st time)   https://stackoverflow.com/a/31361400/7022062  
                        no_test_values_hashmap.merge(no_test_value, 1, (a, b) -> a + b);
                    }
                }

                // It is a "test"! Store it!
                found_tests.add(test_bitmask);

                // Also keep track of how many unique values we found in yes/no rows by given test (how well do the group).
                // We'll find the difference between total "yes" rows minus number of yes_test_values_hashmap.size().
                // When this value is zero it means no 2 same values found (number of value groups == number of rows).
                // When this value is equal to number of rows -1, it means all rows have exact same test value (groupped into a single grouop).
                // (== 0 means no repetition/groupoing, ==1 means 1 groupped set of rows, etc. 
                int groupped_yes_values_count = yes_gold_rows.size() - yes_test_values_hashmap.size();
                // Simmilarly for "no gold":
                int groupped_no_values_count = no_gold_rows.size() - no_test_values_hashmap.size();
                System.out.println("Test found (total: " + found_tests.size()
                        + "). test_bitmask: " + test_bitmask
                        + " test_size: " + test_size
                        + " groupped_yes_values_count: " + groupped_yes_values_count
                        + " groupped_no_values_count: " + groupped_no_values_count
                );
            }  // for: all_permutations_loop
        }  // for: each test_size

        System.out.println("\n--------- Let's find the weights of all our " + headers.size() + " properties ---------");

        // Property weight = (count times property used in tests) / (total number of tests)
        if (found_tests.size() == 0) {
            System.out.println("Can't calculate properties weights: no tests found.");
            return;
        }

        // Reuse bitmask, this time to calculate number of tests having i-th property used
        long test_bitmask = 0;
        for (int i = 0; i < Integer.MAX_VALUE; i++) {
            test_bitmask = test_bitmask == 0
                    ? BinaryUtils.fromBitSet(createFromString("1")) // Convert initial_bit_combination_str string representing a bit sequence into long
                    : BinaryUtils.nextPermutation(test_bitmask);    // generate next test_bitmask permutation

            // Check if it is not last permutation (-1) 
            // or if we have exceeded our max permutations (limited by size of number_of_bits)
            if (test_bitmask == -1 || test_bitmask > max_permutation_size) {
                break;
            }

            long property_used_in_tests_count = 0;
            for (long ith_test : found_tests) {
                if ((ith_test & test_bitmask) == test_bitmask) {
                    property_used_in_tests_count++;
                }
            }
            float weight = (float) property_used_in_tests_count / found_tests.size();
            System.out.println(" " + headers.get(headers.size() - 1 - i) + " has weight:" + weight
                    + " property_used_in_tests_count: " + property_used_in_tests_count
                    + " found_tests.size(): " + found_tests.size()
            );
        }
    }  // main2()
}
